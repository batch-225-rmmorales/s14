// number
let thisIsNum = 5;
console.log(thisIsNum);

// string
let thisIsString = "mors";
console.log(thisIsString);

// array
let thisIsArray = [1,2,3];
console.log(thisIsArray);

// object literal
let thisIsObject = {name:"mors",age:22};
console.log(thisIsObject);

// replicate activity output
function printUserInfo(firstName,lastName,age,hobbies,workAddress){
	console.log("First Name: " + firstName);
	console.log("Last Name: " + lastName);
	console.log("Age: " + age);
	console.log("Hobbies: " + hobbies);
	console.log("Work Address: ");
	console.log(workAddress);

}

let fname = "Rafael";
let lname = "Morales";
let age = 22;
let hobbies = ["gaming", "movies"];
let workAddress = {
	houseNumber : "117", street: "Padre Gomez", city : "QC"
}

printUserInfo(fname,lname,age,hobbies,workAddress);

function returnFunction(){
	return 28;
}

let value = returnFunction();

console.log("value = " + value);

